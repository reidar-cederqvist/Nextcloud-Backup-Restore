# Changelog

## 1.0.0

### General
- Versioning of Nextcloud-Backup-Restore.
- The database system (MySQL/MariaDB or PostgreSQL) is configured in the variable area of the scripts, so it's not necessary to comment/uncomment the specific database commands.
- Special characters for the database password can be used now.
- Single quotes for variables.

### Restore
- The commands for restoring the database are checked at the beginning of the script. Is the specific database system is not installed, the restore is cancelled.
- The default main backup directory now is the same as in the backup script.

## 1.0.1
- Make it fit my system
- Split out settings to config.sh
- Split out several functions to config.sh

## 1.1.0
- Add support for AWS Glacier backup
- Change offsite backup to AWS Glacier
- Removed som unnessecary nl logs
- Fixed bug with relative path to .db-secret
- Fixed invalid rsync instead of rclone check

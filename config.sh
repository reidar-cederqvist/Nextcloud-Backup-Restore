#!/bin/bash

# Variables

# The directory where you store the Nextcloud backups
backupMainDir='/media/hdd/nextcloud_backup'

currentDate=$(date +"%Y%m%d_%H%M%S")

# The actual directory of the current backup - this is a subdirectory of the main directory above with a timestamp
backupdir="${backupMainDir}/${currentDate}/"

# The directory of your Nextcloud installation (this is a directory under your web root)
nextcloudFileDir='/var/www/nextcloud'

# The directory of your Nextcloud data directory (outside the Nextcloud file directory)
# If your data directory is located under Nextcloud's file directory (somewhere in the web root), the data directory should not be a separate part of the backup
nextcloudDataDir='/nextdata'

# The service name of the web server. Used to start/stop web server (e.g. 'systemctl start <webserverServiceName>')
webserverServiceName='apache2'

# Your web server user
webserverUser='www-data'

# The name of the database system (ome of: mysql, mariadb, postgresql)
databaseSystem='mysql'

# Your Nextcloud database name
nextcloudDatabase='nextcloud'

# Your Nextcloud database user
dbUser='nextclouduser'

secret_file='/home/reidar/Nextcloud-Backup-Restore/.db-secret'

# Function for error messages
errorecho() { cat <<< "$@" 1>&2; }

# The password of the Nextcloud database user
if [ ! -f "$secret_file" ]; then
	errorecho "No db secret file available ($secret_file)"
	exit 66
fi
dbPassword="$(cat $secret_file)"

# The maximum number of backups to keep (when set to 0, all backups are kept)
maxNrOfBackups=5

# File names for backup files
fileNameBackupFileDir='nextcloud-filedir.tar.gz'
fileNameBackupDataDir='nextcloud-datadir.tar.gz'
fileNameBackupDb='nextcloud-db.sql'

# Helper functions

# log function
log() { echo -e "$@"; }


DisableMaintenanceMode() {
	log "Switching off maintenance mode..."
	sudo -u "${webserverUser}" /usr/bin/php --define apc.enable_cli=1 ${nextcloudFileDir}/occ maintenance:mode --off
	log "Done"
}

#
# Set maintenance mode
#
EnableMaintenanceMode() {
	log "Set maintenance mode for Nextcloud..."
	sudo -u "${webserverUser}" /usr/bin/php --define apc.enable_cli=1 ${nextcloudFileDir}/occ maintenance:mode --on
	log "Done"
}

# Function to run when Ctrl+c is pressed
CtrlC() {
	read -p "Backup cancelled. Keep maintenance mode? [y/n] " -n 1 -r
	echo

	if ! [[ $REPLY =~ ^[Yy]$ ]]
	then
		DisableMaintenanceMode
		systemctl start "${webserverServiceName}"
	else
		log "Maintenance mode still enabled."
	fi

	exit 1
}

# Function to copy to external storage
#
# zip, passwordprotect and upload latest backup to googleDrive
#
# Not used, uncomment in main backup file for usage
SendToExternalStorage(){
	which rclone 2>&1 >/dev/null || return

	log "Starting Google Drive backup"
	local remote='GdriveEnc:'

	if ! rclone listremotes | grep -q $remote; then
		errorecho "Rclone remote $remote is missing"
		return
	fi
	log "Copy content to Encrypted Google drive"
	sudo -u reidar rclone -v sync $backupdir $remote/ 2>&1

	log "Done"
}

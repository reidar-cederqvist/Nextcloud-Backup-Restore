# Nextcloud-Backup-Restore

This repository contains two bash scripts for backup/restore of [Nextcloud](https://nextcloud.com/).

This is a fork of DecaTec's version [src](https://github.com/DecaTec/Nextcloud-Backup-Restore.git)

## General information

For a complete backup of any Nextcloud instance, you'll have to backup these items:
- The Nextcloud file directory (usually */var/www/nextcloud*)
- The data directory of Nextcloud (it's recommended that this is *not* located in the web root, so e.g. */var/nextcloud_data*)
- The Nextcloud database

The scripts take care of these items to backup automatically.

**Important:**
USE AT YOUR OWN RISK, NO WARRANTY

## Backup

In order to create a backup, simply call the script *NextcloudBackup.sh* on your Nextcloud machine.
the backup is saved in a directory with the current time stamp in your main backup directory. Example: */media/hdd/nextcloud_backup/20170910_132703*.

## Restore

For restore, just call *NextcloudRestore.sh*. This script expects at least one parameter specifying the name of the backup to be restored. In our example, this would be *20170910_132703* (the time stamp of the backup created before). The full command for a restore would be *./NextcloudRestore.sh 20170910_132703*.
You can also specify the main backup directory with a second parameter, e.g. *./NextcloudRestore.sh 20170910_132703 /media/hdd/nextcloud_backup*.

## DB-password
for the scripts to use your database-password correctly, put it in a file called .db-secret

## Offsite Backup
This script supports two ways of uploading the results to a cloud storage

### Rclone
This supports multiple different destinations see [ref](https://en.wikipedia.org/wiki/Rclone)

### AWS Glacier
This script can also try to upload a password-protected compressed version of the results to AWS glacier using pre-saved credentials
please update the nessecary variables in "aws_tool.sh" for this to be propperly done

#!/bin/bash
#
# Bash script for creating backups of Nextcloud.
#
# Version 1.0.1
#
# Usage:
# 	- With backup directory specified in the script:  ./NextcloudBackup.sh
#

scriptFolderPath=$(dirname $(realpath ${BASH_SOURCE[0]}))

source $scriptFolderPath/config.sh
source $scriptFolderPath/aws_tool.sh

log "Backup directory: $backupMainDir"

# Make sure that the drive is mounted
if ! grep -q "/media/hdd" /proc/mounts; then
	sudo mount -a
	# if it is still not mounted -> Exit
	if ! grep -q "/media/hdd" /proc/mounts; then
		log "Harddrive is not mounted, exit to not overfill /"
		exit 999
	fi
fi

# Capture CTRL+C
trap CtrlC INT



#
# Check for root
#
if [ "$(id -u)" != "0" ]
then
	errorecho "ERROR: This script has to be run as root!"
	exit 1
fi

#
# Check if backup dir already exists
#
if [ ! -d "${backupdir}" ]
then
	mkdir -p "${backupdir}"
else
	errorecho "ERROR: The backup directory ${backupdir} already exists!"
	exit 1
fi


#
# Set maintenance mode
#
EnableMaintenanceMode
#
# Stop web server
#
log "Stopping web server..."
systemctl stop "${webserverServiceName}"
log "Done"
echo

#
# Backup file directory
#
log "Creating backup of Nextcloud file directory..."
tar -cpzf "${backupdir}/${fileNameBackupFileDir}" -C "${nextcloudFileDir}" .
log "Done"
echo

#
# Backup data directory
#
log "Creating backup of Nextcloud data directory..."
tar -cpzf "${backupdir}/${fileNameBackupDataDir}"  -C "${nextcloudDataDir}" .
log "Done"
echo

#
# Backup DB
#
if [ "${databaseSystem,,}" = "mysql" ] || [ "${databaseSystem,,}" = "mariadb" ]; then
  	log "Backup Nextcloud database (MySQL/MariaDB)..."

	if ! [ -x "$(command -v mysqldump)" ]; then
		errorecho "ERROR: MySQL/MariaDB not installed (command mysqldump not found)."
		aracter-set=utf8mb4 rrorecho "ERROR: No backup of database possible!"
	else
		mysqldump --single-transaction --default-character-set=utf8mb4 -h localhost -u "${dbUser}" -p"${dbPassword}" "${nextcloudDatabase}" > "${backupdir}/${fileNameBackupDb}"
	fi

	log "Done"
	echo
elif [ "${databaseSystem,,}" = "postgresql" ]; then
	log "Backup Nextcloud database (PostgreSQL)..."

	if ! [ -x "$(command -v pg_dump)" ]; then
		errorecho "ERROR:PostgreSQL not installed (command pg_dump not found)."
		errorecho "ERROR: No backup of database possible!"
	else
		PGPASSWORD="${dbPassword}" pg_dump "${nextcloudDatabase}" -h localhost -U "${dbUser}" -f "${backupdir}/${fileNameBackupDb}"
	fi
	
	log "Done"
	echo
fi

#
# Start web server
#
log "Starting web server..."
systemctl start "${webserverServiceName}"
log "Done"
echo

#
# Disable maintenance mode
#
DisableMaintenanceMode


# Upload the results using rsync
# SendToExternalStorage

# uplaod the results to AWS Glacier
# enable in aws_tool.sh
if [ $AWS_ENABLED -ne 0 ]
then
	log "Starting upload of ${backupdir} to AWS"
	upload_folder ${backupdir}
	clean_backups
	log "Done uploading backup to AWS!"
else
	log "AWS upload not enabled $AWS_ENABLED"
fi

#
# Delete old backups
#
if [ ${maxNrOfBackups} != 0 ]
then
	nrOfBackups=$(ls -l ${backupMainDir} | grep -c ^d)

	if [[ ${nrOfBackups} > ${maxNrOfBackups} ]]
	then
		log "Removing old backups..."
		ls -t ${backupMainDir} | tail -$(( nrOfBackups - maxNrOfBackups )) | while read -r dirToRemove; do
			log "${dirToRemove}"
			rm -r "${backupMainDir}/${dirToRemove:?}"
			log "Done"
			echo
		done
	fi
fi

log "DONE!"
log "Backup created: ${backupdir}"

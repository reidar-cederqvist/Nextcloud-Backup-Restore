#!/bin/bash

AWS_ENABLED=1
AWS_ARCHIVE_IDS_FILE="<db file for normal archive id's>"
AWS_LONG_ARCHIVE_ID_FILE="<db file for long-term archive id's>"
AWS_VAULT_NAME="<vault name>"
AWS_VAULT_REGION="<aws region>"
AWS_NUMBER_OF_BACKUPS=5
AWS_NUMBER_OF_LONG_BACKUPS=2
PASSPHRASE_FILE="<location of your secrets file>"  # the file containing passphrase for encrypting the backup
CHUNK_SIZE="1G"

textToBytes(){
	echo $1 | sed -e 's/t/kg/i;s/g/km/i;s/m/kk/i;s/k/*1024/ig;s/b//i' | bc
}

upload_folder(){
	[ $AWS_ENABLED -eq 0 ] && return
	local folder workdir oldDir encFile treHash fileSize hSize firstFile byteSize month
	local initResponse uploadId curentSize partSize byteStart byteEnd CMUReturn archiveId
	folder="$(realpath $1)"
	workdir="$(dirname $folder)/workdir$(date +%s)"
	mkdir ${workdir}
	oldDir=$(pwd)
	cd $workdir
	echo "encrypting $folder"
	encFile=$workdir/$(basename $folder).tar.gz.gpg
	tar -czPf - ${folder} | gpg -c --batch --passphrase-file $PASSPHRASE_FILE > $encFile
	echo "Calculating AWS TreeHash"
	treeHash=$(awstreehash $encFile | awk '{print $1}')
	fileSize=$(stat --printf="%s" $encFile);
	minSize=$(textToBytes $CHUNK_SIZE)
	if [ $minSize -gt $fileSize ]; then
		echo "folder to small $fileSize aborting"
		cd $oldDir
		rm -rf $workdir
		return
	fi
	# split the file into $CHUNK_SIZE size files
	echo "Split the backup into $CHUNK_SIZE parts"
	split --bytes="$CHUNK_SIZE" $encFile part
	firstFile=$(ls part* | head -1)
	byteSize=$(stat --printf="%s" $firstFile)
	files=$(ls part*)
	echo "files to upload: $files"
	echo "Initiating the multi-part uplaod to AWS"
	initResponse=$(aws glacier initiate-multipart-upload --account-id - --part-size $byteSize --vault-name $AWS_VAULT_NAME --archive-description backup_$(basename $folder))
	uploadId=$(echo $initResponse | jq '.uploadId' | xargs)
	touch commands.txt
	curentSize=0
	for f in $files; do
		partSize=$(stat --printf="%s" $f)
		byteStart=$curentSize
		byteEnd=$((curentSize+partSize-1))
		echo aws glacier upload-multipart-part --body $f --range "'"'bytes '"$byteStart"'-'"$byteEnd"'/'"$fileSize"''"'" --account-id - --vault-name $AWS_VAULT_NAME --region "$AWS_VAULT_REGION"  --upload-id="'"$uploadId"'" >> commands.txt
		curentSize=$((curentSize+partSize))
	done
	parallel -j 50 -a commands.txt --no-notice --bar
	aws glacier list-multipart-uploads --account-id - --vault-name $AWS_VAULT_NAME
	CMUReturn=$(aws glacier complete-multipart-upload --account-id - --vault-name $AWS_VAULT_NAME --upload-id="$uploadId" --archive-size=$fileSize --checksum=$treeHash)
	archiveId=$(echo $CMUReturn | jq .archiveId | xargs)
	if [ ${#archiveId} != 138 ]; then
		echo "invalid archiveId $archiveId"
		cd $oldDir
		rm -rf $workdir
		return
	fi
	echo "Upload done, new archiveId $archiveId added to memory"
	month=$(date +%b)
	day=$(date +%d)
	# if it is the first week of Jun or first week of Jan
	# then make a long term backup
	if [[ "$day" =~ 0[0-7] ]] && [ "$month" == "Jan" -o "$month" == "Jun" ]; then
		echo $archiveId >> $AWS_LONG_ARCHIVE_ID_FILE
	else
		echo $archiveId >> $AWS_ARCHIVE_IDS_FILE
	fi
	cd $oldDir
	rm -rf $workdir
}

clean_backups(){
	[ $AWS_ENABLED -eq 0 ] && return
	# remove normal backups
	if [ -f $AWS_ARCHIVE_IDS_FILE ]; then
		number_of_backups=$(cat $AWS_ARCHIVE_IDS_FILE | wc -l)
		while [ $number_of_backups -gt $AWS_NUMBER_OF_BACKUPS ]; do
			oldest_backup=$(head -1 $AWS_ARCHIVE_IDS_FILE);
			if [ ${#oldest_backup} != 138 ]; then
				echo "Invalid backup archive-id $oldest_backup"
				sed -i '1d' $AWS_ARCHIVE_IDS_FILE
				number_of_backups=$(cat $AWS_ARCHIVE_IDS_FILE | wc -l)
				continue
			fi
			echo removing archive $oldest_backup
			aws glacier delete-archive --account-id - --vault-name $AWS_VAULT_NAME --archive-id $oldest_backup
			sed -i '1d' $AWS_ARCHIVE_IDS_FILE
			number_of_backups=$(cat $AWS_ARCHIVE_IDS_FILE | wc -l)
		done
	fi

	if [ -f $AWS_LONG_ARCHIVE_ID_FILE ]; then
		# remove long-term backups
		number_of_long_term_backups=$(cat $AWS_LONG_ARCHIVE_ID_FILE | wc -l)
		# use if because this will run much more often than the addition to this file
		if [ $number_of_long_term_backups -gt $AWS_NUMBER_OF_LONG_BACKUPS ]; then
			oldest_backup=$(cat $AWS_LONG_ARCHIVE_ID_FILE | head -1);
			if [ ${#oldest_backup} != 138 ]; then
				echo "Invalid backup archive-id $oldest_backup"
				return 1
			fi
			echo removing long-term-archive $oldest_backup
			aws glacier delete-archive --account-id - --vault-name $AWS_VAULT_NAME --archive-id $oldest_backup
			sed -i '1d' $AWS_LONG_ARCHIVE_ID_FILE
		fi
	fi
}
